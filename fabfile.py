from contextlib import contextmanager
from fabric.context_managers import cd, prefix
from fabric.operations import require, run
from fabric.state import env
from fabric.colors import green
from fabric.api import task
from fabconfig import dev


PROVIDED_BY = [dev]


def notify(msg):
    bar = '+' + '-' * (len(msg) + 2) + '+'
    print green('')
    print green(bar)
    print green("| %s |" % msg)
    print green(bar)
    print green('')


@contextmanager
def virtualenv():
    with cd(env.code_dir):
        with prefix("unset PYTHONPATH"):
            with prefix("source %s/bin/activate" % env.virtualenv):
                yield


def django_manage(cmd):
    with virtualenv():
        run('python manage.py %s --settings=%s ' % (cmd, env.settings))


@task
def collectstatic():
    notify("Collecting static")
    django_manage("collectstatic --noinput -v 0")


@task
def restart():
    notify("Restarting application")
    run("appctl restart %s" % env.remote_app_name)


@task
def update_code():
    notify("Updating code")
    with cd(env.code_dir):
        run('git reset --hard')
        run('git pull')


@task
def requirements():
    notify("Installing requirements")
    with virtualenv():
        run('pip install -r %s' % env.requirements)


@task
def syncdb():
    notify("Synchronize db")
    django_manage("syncdb --noinput > /dev/null")
    django_manage("migrate")

    
@task
def deploy():
    require('hosts', provided_by=PROVIDED_BY)
    update_code()
    requirements()
    collectstatic()
    syncdb()
    restart()


@task
def quick_deploy():
    require('hosts', provided_by=PROVIDED_BY)
    update_code()
    collectstatic()
    restart()
