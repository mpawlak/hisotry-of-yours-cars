from django.conf.urls import *
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from untitled.apps.dashboard.views import DashboardView, CarsView, CarDetailView, CarCreateView, CarUpdateView, CarDeleteView
from untitled.apps.dashboard.views import RepairCreateView ,ReplacementCreateView , RefuelingCreateView
from untitled.apps.dashboard.views import RepairDetailView , ReplacementDetailView, RefuelingDetailView
from untitled.apps.dashboard.views import RepairUpdateView, ReplacementUpdateView , RefuelingUpdateView
from untitled.apps.dashboard.views import ReDeleteView
from untitled.apps.account.models import Repair, Replacement , Refueling
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'untitled.views.home', name='home'),
    # url(r'^untitled/', include('untitled.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', DashboardView.as_view(), name='dashboard-home'),
    url(r'^cars/$', CarsView.as_view(), name='dashboard-cars'),
    url(r'^car/add/$', CarCreateView.as_view(), name='car-create'),
    url(r'^car/(?P<pk>[-\d]+)/detail/$', CarDetailView.as_view(), name='car-detail'),
    url(r'^car/(?P<pk>[-\d]+)/update/$', CarUpdateView.as_view(), name='car-update'),
    url(r'^car/(?P<pk>[-\d]+)/delete/$', CarDeleteView.as_view(), name='car-delete'),
    url(r'^car/(?P<pk>[-\d]+)/detail/repair/add/$', RepairCreateView.as_view(), name='repair-create'),
    url(r'^car/(?P<pk>[-\d]+)/detail/replacement/add/$', ReplacementCreateView.as_view(), name='replacement-create'),
    url(r'^car/(?P<pk>[-\d]+)/detail/refueling/add/$', RefuelingCreateView.as_view(), name='refueling-create'),

    url(r'^car/(?P<pk>[-\d]+)/detail/repair/$', RepairDetailView.as_view(), name='repair-detail'),
    url(r'^car/(?P<pk>[-\d]+)/detail/replacement/$', ReplacementDetailView.as_view(), name='replacement-detail'),
    url(r'^car/(?P<pk>[-\d]+)/detail/refueling/$', RefuelingDetailView.as_view(), name='refueling-detail'),

    url(r'^car/(?P<pk>[-\d]+)/detail/repair/update$', RepairUpdateView.as_view(), name='repair-update'),
    url(r'^car/(?P<pk>[-\d]+)/detail/replacement/update$', ReplacementUpdateView.as_view(), name='replacement-update'),
    url(r'^car/(?P<pk>[-\d]+)/detail/refueling/update$', RefuelingUpdateView.as_view(), name='refueling-update'),

    url(r'^car/(?P<pk>[-\d]+)/detail/repair/delete$', ReDeleteView.as_view(model=Repair), name='repair-delete'),
    url(r'^car/(?P<pk>[-\d]+)/detail/replacement/delete$', ReDeleteView.as_view(model=Replacement), name='replacement-delete'),
    url(r'^car/(?P<pk>[-\d]+)/detail/refueling/delete$', ReDeleteView.as_view(model=Refueling), name='refueling-delete'),


)\
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)\
              + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)