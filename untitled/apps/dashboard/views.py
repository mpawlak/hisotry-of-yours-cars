from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404
from untitled.apps.account.models import Car , Repair, Replacement, Refueling
from untitled.apps.account.forms import CarForm, RepairForm , ReplacementForm , RefuelingForm


class DashboardView(TemplateView):
    template_name = "dashboard/dashboard.html"
    active_tab = 'home'

      #Returns a dictionary representing the template context.
    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['active_tab'] = self.active_tab

        return context

class CarsView(ListView):
    template_name = "dashboard/cars.html"
    paginate_by = 10
    model = Car

    active_tab = 'cars'

    def __unicode__(self):
        return self.name

    def get_context_data(self, **kwargs):
        context = super(CarsView, self).get_context_data(**kwargs)
        context['active_tab'] = self.active_tab

        return context



class CarDetailView(DetailView):
    template_name = "dashboard/car_detail.html"
    model = Car

    def get_context_data(self, **kwargs):
        context = super(CarDetailView, self).get_context_data(**kwargs)
        name = get_object_or_404(Car, pk=self.kwargs.get('pk'))
        context['Repair'] = Repair.objects.filter(car=name)
        context['Replacement'] = Replacement.objects.filter(car=name)
        context['Refueling'] = Refueling.objects.filter(car=name)

        return context


class CarCreateView(CreateView):
    template_name = "dashboard/car_form.html"
    model = Car
    form_class = CarForm

    def get_success_url(self):
        return reverse("dashboard-cars")

    def upload_pic(request):
        if request.method == 'POST':
            form = CarForm(request.POST, request.FILES)
            if form.is_valid():
                m = Car.objects.get(pk=Car.pk)
                m.image_car = form.cleaned_data['image_car']
                m.save()

                return HttpResponse('image upload success')
        return HttpResponseForbidden('allowed only via POST')



class CarDeleteView(DeleteView):
    model = Car
    template_name = "dashboard/car_delete.html"

    def get_success_url(self):
        return reverse("dashboard-cars")

class CarUpdateView(UpdateView):
    model = Car
    template_name = "dashboard/car_form.html"

    def get_success_url(self):
        return reverse("dashboard-cars")

class Re(object):
    def get_car(self):
        return get_object_or_404(Car, pk=self.kwargs.get('pk'))

    def form_valid(self, form):
        form.instance.car = self.get_car()
        return super(Re, self).form_valid(form)

    def get_success_url(self):
        return reverse("car-detail" , args=self.kwargs.get('pk'))



class RepairCreateView(Re, CreateView):
    model = Repair
    form_class = RepairForm
    template_name = "dashboard/car_detail/repair_form.html"

class ReplacementCreateView(Re, CreateView):
    model = Replacement
    form_class = ReplacementForm
    template_name = "dashboard/car_detail/replacement_form.html"

class RefuelingCreateView(Re, CreateView):
    model = Refueling
    form_class = RefuelingForm
    template_name = "dashboard/car_detail/refueling_form.html"




class RepairDetailView(DetailView):
    model = Repair
    template_name = "dashboard/car_detail/repair_detail.html"

class ReplacementDetailView(DetailView):
    model = Replacement
    template_name = "dashboard/car_detail/replacement_detail.html"

class RefuelingDetailView(DetailView):
    model = Refueling
    template_name = "dashboard/car_detail/refueling_detail.html"



class RepairUpdateView(Re, UpdateView):
    model = Repair
    form_class = RepairForm
    template_name = "dashboard/car_detail/repair_form.html"

class ReplacementUpdateView(Re, UpdateView):
    model = Replacement
    form_class = ReplacementForm
    template_name = "dashboard/car_detail/replacement_form.html"

class RefuelingUpdateView(Re, UpdateView):
    model = Refueling
    form_class = RefuelingForm
    template_name = "dashboard/car_detail/refueling_form.html"



class ReDeleteView(DeleteView):

    template_name = "dashboard/car_detail/re_delete.html"

    def get_success_url(self):
        return reverse("car-detail", args=(self.object.car.id,))