from django import forms
from untitled.apps.account.models import Car, Repair, Replacement, Refueling


VINTAGE_CHOICES = (('',''),('1980','1980'), ('1981', '1981'), ('1982', '1982'), ('1983', '1983'), ('1984', '1984'), ('1985','1985'), ('1986', '1986'), ('1987', '1987'), ('1988', '1988'), ('1989', '1989'),
('1990','1990'), ('1991', '1991'), ('1992', '1992'), ('1993', '1993'), ('1994', '1994'), ('1995','1995'), ('1996', '1996'), ('1997', '1997'), ('1998', '1998'), ('1999', '1999'),
('2000','2000'), ('2001', '2001'), ('2002', '2002'), ('2003', '2003'), ('2004', '2004'), ('2005','2005'), ('2006', '2006'), ('2007', '2007'), ('2008', '2008'), ('2009', '2009'),
('2010','2000'), ('2011', '2011'), ('2012', '2012'), ('2013', '2013'), ('2014', '2014'),
)



class CarForm(forms.ModelForm):
    class Meta:
        model = Car

    vintage = forms.ChoiceField(choices=VINTAGE_CHOICES)

class RepairForm(forms.ModelForm):

    created = forms.DateTimeField(input_formats=['%m/%d/%Y %H:%M'])
    class Meta:
        model = Repair
        fields = ['title', 'description', 'where', 'price', 'created']

class ReplacementForm(forms.ModelForm):
    created = forms.DateTimeField(input_formats=['%m/%d/%Y %H:%M'])
    class Meta:
        model = Replacement
        fields = ['title', 'description', 'where', 'price', 'created']

class RefuelingForm(forms.ModelForm):
    created = forms.DateTimeField(input_formats=['%m/%d/%Y %H:%M'])
    class Meta:
        model = Refueling
        fields = ['Liters', 'price', 'created']