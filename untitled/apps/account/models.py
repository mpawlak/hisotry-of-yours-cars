from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField
from django.db.models import Sum

class Car(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name=_("name"))
    brand = models.CharField(max_length=255, verbose_name=_("brand") )
    model = models.CharField(max_length=255, verbose_name=_("model"))
    vintage = models.CharField(max_length=4,  verbose_name=_('vintage'))
    mileage = models.FloatField(verbose_name=_('mileage'))
    image_car = ImageField(upload_to='image', blank=True, null=True, verbose_name='image_car')
    created = models.DateTimeField(_('created'), auto_now_add=True)
    updated = models.DateTimeField(_('updated'), auto_now=True)

    class Meta:
        verbose_name = _("Car")
        verbose_name_plural = _("Cars")

    def __unicode__(self):
        return self.name

    def refuelings_cost(self):
        price_sum = self.refueling_set.aggregate(Sum('price'))
        sum = price_sum['price__sum']
        return sum



class RepairAndReplacement(models.Model):
    title = models.CharField(max_length=255, verbose_name=_("title"))
    description = models.CharField(max_length=255, verbose_name=_("description"), blank=True, null=True)
    car = models.ForeignKey('Car', verbose_name=_("car"))
    price = models.FloatField(verbose_name=_("price"))
    where = models.CharField(max_length=255, verbose_name=_("Where"),blank=True, null=True)
    created = models.DateTimeField(_('created'), auto_now_add=False,)

    class Meta:
        verbose_name = _("Repair and Replacement")
        abstract = True


class Repair(RepairAndReplacement):
    pass

class Replacement(RepairAndReplacement):
    pass

class Refueling(models.Model):
    Liters = models.FloatField(verbose_name=_("liters"))
    price = models.FloatField(verbose_name=_("price"))
    car = models.ForeignKey('Car', verbose_name=_("car"))
    created = models.DateTimeField(_('created'), auto_now_add=False)




