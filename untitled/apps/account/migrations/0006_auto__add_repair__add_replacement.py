# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Repair'
        db.create_table(u'account_repair', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Car'])),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('where', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'account', ['Repair'])

        # Adding model 'Replacement'
        db.create_table(u'account_replacement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Car'])),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('where', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'account', ['Replacement'])


    def backwards(self, orm):
        # Deleting model 'Repair'
        db.delete_table(u'account_repair')

        # Deleting model 'Replacement'
        db.delete_table(u'account_replacement')


    models = {
        u'account.car': {
            'Meta': {'object_name': 'Car'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_car': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mileage': ('django.db.models.fields.FloatField', [], {}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vintage': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        u'account.refueling': {
            'Liters': ('django.db.models.fields.FloatField', [], {}),
            'Meta': {'object_name': 'Refueling'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Car']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        u'account.repair': {
            'Meta': {'object_name': 'Repair'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Car']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'where': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'account.replacement': {
            'Meta': {'object_name': 'Replacement'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Car']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'where': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['account']