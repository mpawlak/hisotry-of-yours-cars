# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Costs'
        db.delete_table(u'account_costs')

        # Adding model 'Refueling'
        db.create_table(u'account_refueling', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Liters', self.gf('django.db.models.fields.FloatField')()),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Car'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'account', ['Refueling'])


    def backwards(self, orm):
        # Adding model 'Costs'
        db.create_table(u'account_costs', (
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Car'])),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'account', ['Costs'])

        # Deleting model 'Refueling'
        db.delete_table(u'account_refueling')


    models = {
        u'account.car': {
            'Meta': {'object_name': 'Car'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_car': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mileage': ('django.db.models.fields.FloatField', [], {}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vintage': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        u'account.refueling': {
            'Liters': ('django.db.models.fields.FloatField', [], {}),
            'Meta': {'object_name': 'Refueling'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Car']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['account']