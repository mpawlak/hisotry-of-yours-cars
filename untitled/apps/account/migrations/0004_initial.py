# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Car'
        db.create_table(u'account_car', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('brand', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('model', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('vintage', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('mileage', self.gf('django.db.models.fields.FloatField')()),
            ('image_car', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'account', ['Car'])

        # Adding model 'Costs'
        db.create_table(u'account_costs', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Car'])),
            ('price', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'account', ['Costs'])


    def backwards(self, orm):
        # Deleting model 'Car'
        db.delete_table(u'account_car')

        # Deleting model 'Costs'
        db.delete_table(u'account_costs')


    models = {
        u'account.car': {
            'Meta': {'object_name': 'Car'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_car': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'mileage': ('django.db.models.fields.FloatField', [], {}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vintage': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        u'account.costs': {
            'Meta': {'object_name': 'Costs'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Car']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['account']