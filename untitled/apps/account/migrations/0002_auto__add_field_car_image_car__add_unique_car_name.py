# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Car.image_car'
        db.add_column(u'nowa_car', 'image_car',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True),
                      keep_default=False)

        # Adding unique constraint on 'Car', fields ['name']
        db.create_unique(u'nowa_car', ['name'])


    def backwards(self, orm):
        # Removing unique constraint on 'Car', fields ['name']
        db.delete_unique(u'nowa_car', ['name'])

        # Deleting field 'Car.image_car'
        db.delete_column(u'nowa_car', 'image_car')


    models = {
        u'nowa.car': {
            'Meta': {'object_name': 'Car'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_car': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'mileage': ('django.db.models.fields.FloatField', [], {}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vintage': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        u'nowa.costs': {
            'Meta': {'object_name': 'Costs'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['nowa.Car']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['nowa']