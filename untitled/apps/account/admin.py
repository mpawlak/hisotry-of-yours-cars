from django.contrib import admin
from untitled.apps.account import models

admin.site.register(models.Car)

admin.site.register(models.Refueling)