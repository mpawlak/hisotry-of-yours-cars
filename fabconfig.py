from fabric.decorators import task
from fabric.state import env


@task
def dev():
    env.hosts = ['cars.bluecase.pl']
    env.user = 'cars'
    env.remote_app_name = "cars"
    env.code_dir = '/home/cars/www/app'
    env.static_dir = env.code_dir + '/static'
    env.virtualenv = '/home/cars/www/.envs/cars'
    env.requirements = 'requirements.txt'
    env.settings = 'untitled.settings'
